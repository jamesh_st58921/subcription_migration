
# Sample Commands 

```
.\auto-sub-resourcemover_insight.ps1  -dry -s "6ecd9a5e-cd8a-4317-9987-03fdfaba26e4" -d "8e328049-1bc4-4c8c-8d1f-8f61d3fd9953" -t "172c6c70-30c8-49a7-bf31-32d635c6f507" 
```

#### Arguments: 
```
"-dry":  Optional. Dry run mode. If specified, it will not execute resource move
"-s":  Required. Source (old) subscription
"-d":  Required. Destination (new) subscription
"-t":  Required. Tenant ID
```

---
**Output:**
Output CSV file and log are saved to ../logs/[subscription_id] folder 

---

